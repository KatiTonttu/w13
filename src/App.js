import './App.css';

function App() {
  const names = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  //here is an example how to use the array above to create an array of option elements
  //see https://www.w3schools.com/jsref/jsref_map.asp
  const arrayOfTableNames = names.map((name)=>{
    return <tr><td>{name}</td></tr>

  })
  return (
    <div className="App">
      <hr/>
      <div>
        <table>
          <thead>
          <tr>
          <th>Weekday</th>
          </tr>
          </thead>
          <tbody class ="days">
          {arrayOfTableNames}
          </tbody>
          </table>
      </div>
    </div>
  );
}

export default App;
